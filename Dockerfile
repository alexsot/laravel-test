FROM shakyshane/laravel-app:latest

WORKDIR /var/www

COPY database /var/www/database

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \ 
    && chmod 755 composer-setup.php 
RUN php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && php composer.phar install --no-dev --no-scripts \
    && rm composer.phar

COPY . /var/www

RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache

RUN php artisan optimize

